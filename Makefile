# Device
DEVICE = STM32F411xE
SERIES_CPU  = cortex-m4
SERIES_ARCH = armv7e-m+fp
MAPPED_DEVICE = $(DEVICE)

# Flash
FLASH  = 0x08000000
FLASHTING_TOOL = st-flash

# Project folders
SRC_FOLDER ?= src
DRIVER_FOLDER ?= driver
CMSIS_FOLDER ?= $(DRIVER_FOLDER)/cmsis
INC_FOLDER ?= $(CMSIS_FOLDER)/inc
LINKER_FOLDER ?= $(DRIVER_FOLDER)/linker
STARTUP_FOLDER ?= $(DRIVER_FOLDER)/startup
BUILD_FOLDER ?= build
BIN_FOLDER ?= $(BUILD_FOLDER)/bin
OBJ_FOLDER ?= $(BUILD_FOLDER)/obj

# Toolchain
CC 			= arm-none-eabi-gcc
OBJCOPY 	= arm-none-eabi-objcopy
LD      	= arm-none-eabi-ld -v 		# Not used
OBJDUMP 	= arm-none-eabi-objdump     # Not used
SIZE    	= arm-none-eabi-size    	# Not used

# Flags - C Language Options
CFLAGS += -ffreestanding
# Flags - Overall Options
CFLAGS += -specs=nosys.specs
# Flags - Warning Options
CFLAGS += -Wall
CFLAGS += -Wextra
# Flags - Debugging Options
CFLAGS += -g
# Flags - Optimization Options
CFLAGS += -ffunction-sections
CFLAGS += -fdata-sections
# Flags - Preprocessor options
CFLAGS += -D $(MAPPED_DEVICE)
# Flags - Assembler Options
CFLAGS += -Wa,--defsym,CALL_ARM_SYSTEM_INIT=1
# Flags - Linker Options
CFLAGS += -Wl,-L$(LINKER_FOLDER),-T$(LINKER_FOLDER)/$(MAPPED_DEVICE).ld
# Flags - Include Options
CFLAGS += -I$(INC_FOLDER)
CFLAGS += -I$(STARTUP_FOLDER)
# Flags - Machine-dependant options
CFLAGS += -mcpu=$(SERIES_CPU)
CFLAGS += -march=$(SERIES_ARCH)
CFLAGS += -mlittle-endian
CFLAGS += -mthumb
# CFLAGS += -masm-syntax-unified #TODO: clangd doesn't like this argument

# Output files
ELF_FILE_NAME ?= main.elf
BIN_FILE_NAME ?= main.bin
OBJ_FILE_NAME ?= startup_$(MAPPED_DEVICE).o
ELF_FILE_PATH = $(BIN_FOLDER)/$(ELF_FILE_NAME)
BIN_FILE_PATH = $(BIN_FOLDER)/$(BIN_FILE_NAME)
OBJ_FILE_PATH = $(OBJ_FOLDER)/$(OBJ_FILE_NAME)

# Input files
SRC ?=
SRC += $(SRC_FOLDER)/*.c

# CMSIS source files
SRC += $(CMSIS_FOLDER)/src/*.c

# Startup file
DEVICE_STARTUP = $(STARTUP_FOLDER)/$(MAPPED_DEVICE).s

# Make targets
.DEFAULT_GOAL :=  all
all:$(BIN_FILE_PATH)

$(BIN_FILE_PATH): $(ELF_FILE_PATH)
	$(OBJCOPY) -O binary $^ $@

$(ELF_FILE_PATH): $(SRC) $(OBJ_FILE_PATH) | $(BIN_FOLDER)
	$(CC) $(CFLAGS) $^ -o $@

$(OBJ_FILE_PATH): $(DEVICE_STARTUP) | $(OBJ_FOLDER)
	$(CC) -c $(CFLAGS) $^ -o $@

$(BIN_FOLDER):
	mkdir $(BIN_FOLDER)

$(OBJ_FOLDER): $(BUILD_FOLDER)
	mkdir $(OBJ_FOLDER)

$(BUILD_FOLDER):
	mkdir $(BUILD_FOLDER)

# Make clean
clean:
	rm -f $(ELF_FILE_PATH)
	rm -f $(BIN_FILE_PATH)
	rm -f $(OBJ_FILE_PATH)

# Make flash
flash: all
	st-flash write $(BIN_FOLDER)/$(BIN_FILE_NAME) $(FLASH)

.PHONY: all clean flash
