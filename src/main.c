#include "stm32f411xe.h"

#define GPIOAEN                 (1U<<0)
#define PIN5                    (1U<<5)

int main(void) {
    // Enable clock access to GPIOA
    RCC->AHB1ENR |= GPIOAEN;

    // Set PA5 as output pin
    GPIOA->MODER |= (1U<<10);     // Set bit 10 to 1 */
    GPIOA->MODER &=~ (1U<<11);    // Set bit 11 to 0 */

    while(1) {
        // Toggle bit
        GPIOA->ODR ^= PIN5;
        for(int i=0; i<1000000;i++){};
    }
}
