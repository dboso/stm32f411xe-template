# STM32F411xE template

Minimal project template for bare metal STM32F411xE C programming

## Requirements

* [GNU Arm Embedded Toolchain](https://developer.arm.com/downloads/-/gnu-rm) 
* [stlink](https://github.com/stlink-org)
* [make](https://www.gnu.org/software/make/)

## Run

Compile

``` 
make
```

Flash

``` 
make flash
```
