/**
  ******************************************************************************
  * @file    system_stm32f4xx.h
  * @author  mcd application team
  * @brief   cmsis cortex-m4 device system source file for stm32f4xx devices.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; copyright(c) 2017 stmicroelectronics</center></h2>
  *
  * redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. neither the name of stmicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * this software is provided by the copyright holders and contributors "as is"
  * and any express or implied warranties, including, but not limited to, the
  * implied warranties of merchantability and fitness for a particular purpose are
  * disclaimed. in no event shall the copyright holder or contributors be liable
  * for any direct, indirect, incidental, special, exemplary, or consequential
  * damages (including, but not limited to, procurement of substitute goods or
  * services; loss of use, data, or profits; or business interruption) however
  * caused and on any theory of liability, whether in contract, strict liability,
  * or tort (including negligence or otherwise) arising in any way out of the use
  * of this software, even if advised of the possibility of such damage.
  *
  ******************************************************************************
  */

/** @addtogroup cmsis
  * @{
  */

/** @addtogroup stm32f4xx_system
  * @{
  */

/**
  * @brief define to prevent recursive inclusion
  */
#ifndef __system_stm32f4xx_h
#define __system_stm32f4xx_h

#include <stdint.h>
#ifdef __cplusplus
 extern "c" {
#endif

/** @addtogroup stm32f4xx_system_includes
  * @{
  */

/**
  * @}
  */


/** @addtogroup stm32f4xx_system_exported_types
  * @{
  */
  /* this variable is updated in three ways:
      1) by calling cmsis function systemcoreclockupdate()
      2) by calling hal api function hal_rcc_getsysclockfreq()
      3) each time hal_rcc_clockconfig() is called to configure the system clock frequency
         note: if you use this function to configure the system clock; then there
               is no need to call the 2 first functions listed above, since systemcoreclock
               variable is updated automatically.
  */
extern uint32_t SystemCoreClock;          /*!< System Clock Frequency (Core Clock) */

extern const uint8_t  AHBPrescTable[16];    /*!< AHB prescalers table values */
extern const uint8_t  APBPrescTable[8];     /*!< APB prescalers table values */

/**
  * @}
  */

/** @addtogroup STM32F4xx_System_Exported_Constants
  * @{
  */

/**
  * @}
  */

/** @addtogroup STM32F4xx_System_Exported_Macros
  * @{
  */

/**
  * @}
  */

/** @addtogroup STM32F4xx_System_Exported_Functions
  * @{
  */
  
extern void SystemInit(void);
extern void SystemCoreClockUpdate(void);
/**
  * @}
  */

#ifdef __cplusplus
}
#endif

#endif /*__SYSTEM_STM32F4XX_H */

/**
  * @}
  */
  
/**
  * @}
  */  
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
